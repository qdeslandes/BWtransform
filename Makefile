SHELL = /bin/sh

CC       = gcc
CFLAGS   = -W -Wall -Wpedantic -Wextra -Werror -std=c99 -g
BUILDDIR = bin
SRCSDIR  = src
SRCS     = $(wildcard $(SRCSDIR)/*.c)
OBJSDIR  = bin
OBJS     = $(patsubst $(SRCSDIR)/%.c, $(OBJSDIR)/%.o, $(SRCS))
LIBS     =
INCLUDES = -Iincludes

all: main

### Main target, build project
main: $(OBJS)
	$(CC) -o $(BUILDDIR)/out $^ $(LIBS) $(INCLUDES)

$(BUILDDIR)/%.o: $(SRCSDIR)/%.c
	$(CC) $(CFLAGS) -c $^ -o $@ $(INCLUDES)

### Clean linked binary
clean:
	rm -rf $(BUILDDIR)/out

###Remove everything
mrproper:
	rm -rf $(BUILDDIR)/*

