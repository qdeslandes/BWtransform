#include "reverse_burrows_wheeler.h"

/* frequencies : count frequency of each letter in t */
int *frequencies(const char *t, int n)
{
	int *freq = malloc(256 * sizeof(int));
	
	if (! freq) {
		printf("ERROR : Could not allocate memory for frequencies.\n");
		return NULL;
	}

	memset(freq, 0, 256 * sizeof(int));

	for(int i = 0; i < n; ++i) {
		int val = (int)t[i];
		++freq[val];	
	}

	return freq;
}

/* sortchar : return a sorted copy of t */
char *sortchar(const char *t, int n)
{
	int *freq = frequencies(t, n);

	if (! freq) {
		printf("ERROR : No frequencies array.\n");
		return NULL;
	}

	char *sorted_t = malloc(n * sizeof(char));

	if (! sorted_t) {
		free(freq);
		printf("ERROR : Could not allocate memory for sorted array.\n");
		return NULL;
	}
	
	int index = 0;

	for(int i = 0; i < 256; ++i) {
		int occur = freq[i];
		
		for(int j = 0; j< occur; ++j) {
			sorted_t[index] = i;
			++index;
		}	
	}

	free(freq);

	return sorted_t;
}

/* find : find c in t[] */
static int find(char c, char *t, int n)
{
        for(int i = 0; i < n; ++i) {
                if (t[i] == c) {
                        t[i] = (char)0;
                        return i;
                }
        }

        return -1;
}

/* find_indexes : find index of sorted_t[i] in t */
int *find_indexes(const char *t, int n)
{
        char t_keyless[n-1];
        strncpy(t_keyless, t, n-1);
        
        char *sorted_t = sortchar(t_keyless, n-1);
        
	if (! sorted_t) {
		printf("ERROR : No sorted array.\n");
		goto err;
	}

	int *indexes = malloc((n-1) * sizeof(int));

	if (! indexes) {
		free(sorted_t);
		printf("ERROR : Could not allocate memory for indexes array.\n");
		goto err;
	}
 
        for(int i = 0; i < n-1; ++i)
                indexes[i] = find(sorted_t[i], t_keyless, n-1);
        
        free(sorted_t);
        return indexes;

err:
	return NULL;
}

/* bwreverse : operate the Burrows-Wheeler reverse transform of the string t */
char *bwreverse(const char *t, int n)
{
        char *t_final = malloc((n-1) * sizeof(char));
 
	if (! t_final) {
		printf("ERROR : Could not allocate array for final text.\n");
		goto err;
	}
       
        int *indexes = find_indexes(t, n);

	if (! indexes) {
		free(t_final);
		printf("ERROR : No indexes array available.\n");
		goto err;
	}

        for(int i = 0, index = t[n-1]; i < n-1; ++i) {
                t_final[i] = t[index];
                index = indexes[index];
        }

        free(indexes);
        return t_final;

err:
	return NULL;
}
