#ifndef _REDUNDANCY_COMPRESS_H
#define _REDUNDANCY_COMPRESS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compute_marker(const char *t, int n);
int *occurences(const char *t, int n);
int compressed_size(const char *t, int n);
char *compress(const char *t, int n, int *rsize);

#endif
